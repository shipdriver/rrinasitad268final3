﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace RRinas_ITAD268_Final_Q3
{
    class Program
    {
        static void Main(string[] args)
        {
            double rate = 5;
            double term = 10;
            double amount = 100000;
            
            Console.WriteLine(Financial.Pmt(rate / 100 / 12, term * 12, -amount).ToString("n2"));
            Console.ReadLine();
        }
    }
}
